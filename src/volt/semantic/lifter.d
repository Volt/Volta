// Copyright © 2016, Jakob Bornecrantz.  All rights reserved.
// See copyright notice in src/volt/license.d (BOOST ver. 1.0).
module volt.semantic.lifter;

import ir = volt.ir.ir;
import ircopy = volt.ir.copy;

import volt.interfaces;

	
/**
 * IR Lifter, aka Liftertron3000, copies and does transformations on IR.
 *
 * This is the base class providing utility functions and a common interface
 * for users. One reason for lifting is that the target we are compiling for
 * is not the same as for the compiler is running on, and as such different
 * backend transformations needs to be done on the IR. Extra validation can
 * also be done while copying.
 *
 * Dost thou even lyft brother.
 */
abstract class Lifter
{
public:
	LanguagePass lp;

private:
	ir.Function[ir.NodeID] mStore;
	ir.Module mMod;
	ir.Module[] mMods;

public:
	this(LanguagePass lp)
	{
		this.lp = lp;
	}

	/**
	 * Resets the lifter and clears all cached functions and modules.
	 */
	void reset()
	{
		version (Volt) {
			mStore = [];
		} else {
			mStore = null;
		}
		mMod = null;
		mMods = null;
	}

	/**
	 * Create a new module to store functions in.
	 *
	 * Does not clear the function cache, so functions can refer
	 * to functions in earlier modules.
	 */
	ir.Module newModule()
	{
		mMod = new ir.Module();
		mMods ~= mMod;

		return mMod;
	}

	/**
	 * Lift or returns a cached copy of the given function.
	 */
	ir.Function lift(ir.Function func)
	{
		assert(mMod !is null);

		ir.Function ret;
		if (mStore.get(func.uniqueId, ret)) {
			return ret;
		}

		return doLift(func);
	}

protected:
	/**
	 * Copies the function declration and adds it to the store.
	 *
	 * The body, in and out contracts are left null and will need to be
	 * copied by the caller. Intended to be used as a helper function.
	 */
	ir.Function copyFunction(ir.Function old)
	{
		assert((old.uniqueId in mStore) is null);

		// TODO Need actualize as insted.
		//lp.actualize(old);
		assert(old.isActualized);

		auto func = new ir.Function();
		func.location = old.location;
		func.isResolved = old.isResolved;
		func.isActualized = old.isActualized;
		func.access = old.access;
		// SCOPE COPY HERE
		func.kind = old.kind;
		func.type = new ir.FunctionType(old.type);
		version (Volt) {
			func.params = new old.params[0 .. $];
			func.nestedFunctions = new old.nestedFunctions[0 .. $];
			func.scopeSuccesses = new old.scopeSuccesses[0 .. $];
			func.scopeExits = new old.scopeExits[0 .. $];
			func.scopeFailures = new old.scopeFailures[0 .. $];
		} else {
			func.params = old.params.dup;
			func.nestedFunctions = old.nestedFunctions.dup;
			func.scopeSuccesses = old.scopeSuccesses.dup;
			func.scopeExits = old.scopeExits.dup;
			func.scopeFailures = old.scopeFailures.dup;
		}
		foreach (i, param; old.params) {
			func.params[i] = copyFunctionParam(param, func);
		}
		foreach (i, nfunc; old.nestedFunctions) {
			func.nestedFunctions[i] = copyFunction(nfunc);
		}
		foreach (i, scopeFunc; old.scopeSuccesses) {
			func.scopeSuccesses[i] = copyFunction(scopeFunc);
		}
		foreach (i, scopeFunc; old.scopeExits) {
			func.scopeExits[i] = copyFunction(scopeFunc);
		}
		foreach (i, scopeFunc; old.scopeFailures) {
			func.scopeExits[i] = copyFunction(scopeFunc);
		}
		func.name = old.name;
		func.mangledName = old.mangledName;  // TODO: Prefix
		func.suffix = old.suffix;
		func.outParameter = old.outParameter;
		// BLOCK STATEMENTS HERE
		if (old.thisHiddenParameter !is null) {
			func.thisHiddenParameter = copyVariable(old.thisHiddenParameter);
		}
		if (old.nestedHiddenParameter !is null) {
			func.nestedHiddenParameter = copyVariable(old.nestedHiddenParameter);
		}
		if (old.nestedVariable !is null) {
			func.nestedVariable = copyVariable(old.nestedVariable);
		}
		// NEST STRUCT
		func.isWeakLink = old.isWeakLink;
		func.vtableIndex = old.vtableIndex;
		func.loadDynamic = old.loadDynamic;
		func.isMarkedOverride = old.isMarkedOverride;
		func.isOverridingInterface = old.isOverridingInterface;
		func.isAbstract = old.isAbstract;
		func.isAutoReturn = old.isAutoReturn;
		func.isLoweredScopeExit = old.isLoweredScopeExit;
		func.isLoweredScopeFailure = old.isLoweredScopeFailure;
		func.isLoweredScopeSuccess = old.isLoweredScopeSuccess;
		// TODO more fields
		mStore[old.uniqueId] = func;
		return func;
	}

	ir.Node copyNode(ir.Node n)
	{
		assert(false);
	}

	ir.FunctionParam copyFunctionParam(ir.FunctionParam old, ir.Function newFunction)
	{
		auto fparam = new ir.FunctionParam();
		fparam.location = old.location;
		fparam.func = newFunction;
		fparam.index = old.index;
		if (old.assign !is null) {
			fparam.assign = ircopy.copyExp(old.assign);
		}
		fparam.name = old.name;
		fparam.hasBeenNested = old.hasBeenNested;
		return fparam;
	}

	ir.TopLevelBlock copyTopLevelBlock(ir.TopLevelBlock old)
	{
		auto tlb = new ir.TopLevelBlock();
		tlb.location = old.location;
		version (Volt) {
			tlb.nodes = new old.nodes[0 .. $];
		} else {
			tlb.nodes = old.nodes.dup;
		}
		foreach (i, node; old.nodes) {
			tlb.nodes[i] = copyNode(node);
		}
		return tlb;
	}

	ir.UserAttribute copyUserAttribute(ir.UserAttribute old)
	{
		assert(false);
	}

	ir.Variable copyVariable(ir.Variable old)
	{
		assert(false);
	}

	/**
	 * Implemented by child classes, copies the Function into the
	 * current module mMod and applies error checking and transformation
	 * needed for that specific lifter.
	 */
	abstract ir.Function doLift(ir.Function);
}

class CTFELifter : Lifter
{
public:
	this(LanguagePass lp)
	{
		super(lp);
	}


protected:
	override ir.Function doLift(ir.Function old)
	{
		// Copy declaration and add function to store.
		auto func = copyFunction(old);

		// TODO copy in, out and body.

		return func;
	}

	override ir.Function copyFunction(ir.Function old)
	{
		auto func = super.copyFunction(old);
		func.mangledName = "__CTFE_" ~ func.mangledName;
		return func;
	}
}

/+
void runExp(ref ir.Exp exp)
{
	rexp := cast(ir.RunExp) exp;
	pfix := cast(ir.Postfix) rexp.value;
	func := getFunctionFromPostfix(pfix);
	dlgt := lp.liftFunction(func);
	static is (typeof(dlgt) == ir.Constant delegate(ir.Constant[] args));

	args : ir.Constant[];
	foreach (exp; pfix.arguments) {
		args ~= evaluate(exp);
	}

	dlgt(args);
}
+/
